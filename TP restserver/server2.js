"use strict";

const express = require('express');
const app = express();
const morgan = require('morgan');
const fs = require('fs');
const path = require('path');
const bodyParser = require('body-parser');

//DB representation in javascript
let DB;

if(process.argv.length === 2){
    console.log("Please provide a port argument");
}else{
    let port = process.argv[2];
    loadDB();
    //Express functions
    app.use(bodyParser.json());
    app.use(morgan("tiny"));
    //Pages served by the server
    app.get('/', (req, res) => res.send('Hi'));
    app.get('/exit',(req, res) => exit(req, res));
    app.get('/restore',(req, res) => restore(req, res));
    app.get('/papercount',(req,res) =>countPapers(req, res));
    app.get('/author/:authorName',(req, res) => countPapersByAuthor(req,res));
    app.get('/papersfrom/:authorName',(req, res) => returnPapersFromAuthor(req, res));
    app.get('/titles/:authorName',(req, res) => returnTitleFromAuthor(req, res));
    app.use('/reference/:key',(req,res) => referencesMiddleWareProcessing(req,res));
    app.post('/reference',(req,res) => postReference(req,res));
    app.listen(port, () => console.log('Example app listening on port '+ port+'!'));
}

function loadDB(){
    const DBtext = fs.readFileSync(path.join(__dirname,"db.json"),"utf8");
    DB = JSON.parse(DBtext);
}

function exit(req,res){
    res.send("Server will now exit");
    process.exit(0);
}

function restore(req, res){
    loadDB();
    res.set('Content-Type','text/plain');
    res.send("db.json reloaded");
}

function countPapers(req,res){
    res.set('Content-Type','text/plain');
    res.send(DB.length.toString());
}

function countPapersByAuthor(req, res) {
    const partialAuthorName =req.params["authorName"];
    let count = papersByAuthors(partialAuthorName).length;
    res.set('Content-Type','text/plain');
    res.send(count.toString());
}

function papersByAuthors(partialAuthorName){
    let lowercasePartialAuthorName = partialAuthorName.toLowerCase();
    let publications = [];
    for(let publication of DB){
        let authors = publication.authors;
        for(let author of authors){
            let authorName = author.toLowerCase();
            if(authorName.includes(lowercasePartialAuthorName)){
                publications.push(publication);
            }
        }
    }
    return publications;
}

function returnPapersFromAuthor(req,res){
    const partialAuthorName =req.params["authorName"];
    let publications = papersByAuthors(partialAuthorName);
    res.set('Content-Type','application/json');
    res.send(JSON.stringify(publications));
}

function returnTitleFromAuthor(req, res){
    const partialAuthorName = req.params["authorName"];
    let publications = papersByAuthors(partialAuthorName);
    let titles = [];
    for(let publication of publications){
        titles.push(publication.title);
    }
    res.set('Content-Type','application/json');
    res.send(JSON.stringify(titles));
}

function referencesMiddleWareProcessing(req, res){
    let key = req.params["key"];
    if(req.method === 'GET'){
        getReference(req, res, key);
    }else if(req.method === 'DELETE'){
        deleteReference(req,res,key);
    }else if(req.method === 'PUT'){
        putReference(req, res, key);
    }
}

function publicationIDByKey(key){
    for(let publicationIndex in DB){
        if(DB[publicationIndex].key === key){
            return publicationIndex;
        }
    }
}

function getReference(req, res,key){
    let publicationByKey = DB[publicationIDByKey(key)];
    res.set('Content-Type','application/json');
    res.send(JSON.stringify(publicationByKey));
}

function deleteReference(req, res, key){
    DB.splice(publicationIDByKey(key),1);
    res.set('Content-Type','application/json');
    res.send(DB);
}

function postReference(req, res){
    if(req.method !== 'POST'){
        res.status(404).send('Not found');
    }
    let newPublication = req.body;
    newPublication.key = "imaginary";
    DB.push(newPublication);
    res.set('Content-Type','application/json');
    res.send(DB);
}

function putReference(req, res, key){
    const publicationModifications= req.body;
    let publicationModificationKeys = Object.keys(publicationModifications);
    const id = publicationIDByKey(key);
    for(let newKeys of publicationModificationKeys){
        DB[id][newKeys] = publicationModifications[newKeys];
    }
    res.set('Content-Type','application/json');
    res.send(DB);
}
