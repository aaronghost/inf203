"use strict";

let http = require('http');
let url = require('url');
let fs = require('fs');
let path = require('path');
let queryString = require('querystring');

if(process.argv.length === 2){
    console.log("Please provide a port argument");
}else{
    let port = process.argv[2];
    console.log("Server launched on port " + port);
    http.createServer(generateServerResponse).listen(port);
}

function generateServerResponse(req, res){
    try {
        let query = url.parse(req.url);
        //case of the root path
        if (query.pathname === '/') {
            res.writeHead(200, {'Content-Type': 'text/html'});
            res.write("This is a valid server address.");
            res.end();
        } else {
            //case of all the other instructions
            const queryRegister = query.pathname.split("/")[1];
            console.log("Function asked : "+queryRegister);
            //exit
            if (queryRegister === "exit") {
                res.writeHead(200, {'Content-Type': 'text/html'});
                res.write("The server will stop now.");
                res.end(exitCallBack);
                //file serving
            } else if(queryRegister === "files"){
                const local_path = path.join(__dirname,query.pathname.substring(6));
                //Checking for root dir in the normalized path
                if(local_path.indexOf(__dirname) !== 0){
                    raise404(res);
                }else {
                    const file_exists = fs.existsSync(local_path);
                    console.log("File : " + local_path + " status on server : " + file_exists);
                    //If it does not exist, raise 404
                    if (file_exists === false) {
                        raise404(res);
                    }
                    //Else serve the page
                    else {
                        //Get file content
                        const fileContent = fs.readFileSync(local_path);
                        //get MIME type for file
                        const MIMEtype = returnCorrectMime(local_path);
                        //write correct response
                        res.writeHead(200, {'Content-Type': MIMEtype})
                        res.write(fileContent);
                        res.end();
                    }
                }
            }else  if(queryRegister === "hello") {
                res.writeHead(200, {'Content-Type': 'text/html'});
                let dictionary = convertParameters(queryString.unescape(query.search));
                res.write("Hello " + dictionary["name"]);
                res.end();
            }else if(queryRegister === "hello2") {
                res.writeHead(200, {'Content-Type': 'text/html'});
                let dictionary = convertParameters(queryString.unescape(query.search));
                let bodyResponse = constructHello2Page(sanitize(dictionary["name"]));
                res.write(bodyResponse,'utf-8');
                res.end();
            }else if(queryRegister === "clear"){
                fs.unlinkSync(".names.json");
                res.writeHead(200,{'Content-Type': 'text/html'});
                res.write("Cleared all Hello2 entries");
                res.end();
            } else{
                raise404(res);
            }
        }
    }catch(error) {
        console.error(error);
    }
}

function sanitize(str){
    return str.replace(/<[^>]*>?/gm, '');
}

function constructHello2Page(nameParameter){
    let previousMembers = "";
    let names = [];
    if(fs.existsSync(path.join(__dirname,".names.json"))){
        names = JSON.parse(fs.readFileSync(path.join(__dirname,".names.json")));
        for(let nameIndex in names){
            previousMembers +=  names[nameIndex];
            if(parseInt(nameIndex) !== names.length-1){
                previousMembers += ", ";
            }
        }
    }
    let responseBody ="Hello " +nameParameter + ", the following users have already visited this page: " + previousMembers;
    //Add names to the end of the JSON and save it
    names.push(nameParameter);
    fs.writeFileSync(path.join(__dirname,".names.json"),JSON.stringify(names));
    return responseBody;
}

function convertParameters(str){
    let parameterDictionary = {};
    const stringWithoutQuestionMark = str.substring(1);
    const parameterStrings = stringWithoutQuestionMark.split("&");
    for(const parameterString of parameterStrings){
        const keyValue = parameterString.split("=");
        console.log("Parameter in the query : "+keyValue[0] +" of value : "+keyValue[1]);
        parameterDictionary[keyValue[0]] = keyValue[1];
    }
    return parameterDictionary;
}

function exitCallBack(){
    process.exit(0);
}

function raise404(res){
    res.writeHead(404,{'Content-Type':'text/html'});
    res.write("Page not found on this server");
    res.end();
}

function returnCorrectMime(str){
    const splittedTypes = str.split(".");
    const last_extension = splittedTypes[splittedTypes.length - 1];
    switch (last_extension) {
        case "htm":
        case "html":
            return "text/html";
        case "css":
            return "text/css";
        case "jpg":
        case "jpeg":
            return "image/jpeg";
        case "js":
            return "application/javascript";
        case "gif":
            return "image/gif";
        case "json":
            return "application/json";
        case "pdf":
            return "application/pdf";
        case "txt":
            return "text/plain";
        case "zip":
            return "application/zip";
    }
}

