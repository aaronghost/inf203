"use strict";

function loadJson(){
    let request = new XMLHttpRequest();
    request.open('GET','slides.json');
    request.onload = function(){
        let slides =JSON.parse(request.responseText);
        for (let slide of slides.slides){
                setTimeout(function () {
                    retrievePicturefromfile(slide.url)
                }, 1000 * slide.time);
        }
    };
    request.send();
}

function retrievePicturefromfile(url){
    if(url !== "") {
        document.getElementById("MAIN").innerHTML = "<iframe src=" + url + "> </iframe>";
    }else{
        document.getElementById("MAIN").innerHTML = "";
    }
}