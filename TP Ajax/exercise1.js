"use strict";

function loadDoc(){
    let request = new XMLHttpRequest();
    request.open("GET","text.txt",false);
    request.onreadystatechange = function (){
        if(request.readyState === 4){
            if(request.status === 200){
                document.getElementById("textarea").innerText = request.responseText;
            }
        }
    };
    request.send();
}

function loadDoc2(){
    let request = new XMLHttpRequest();
    request.open("GET","text.txt",false);
    request.onreadystatechange = function (){
        if(request.readyState === 4){
            if(request.status === 200 || request.status === 0){
                let texts = request.responseText.split("<br\/>");
                for(const text of texts) {
                    let lign = "<p style =\"color:" + getRandomColor() + ";\">" + text + "<\/p>";
                    document.getElementById("textarea2").innerHTML += lign;
                }
            }
        }
    };
    request.send();
}

function getRandomColor() {
    const letters = '0123456789ABCDEF';
    let color = '#';
    for (let i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}


