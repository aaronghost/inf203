"use strict";

window.onload = loadJson;
var intervals = [];
var currentStep = -1;
var slides;
var stopped = false;

function loadJson(){
    let request = new XMLHttpRequest();
    request.open('GET','slides.json');
    request.onload = function(){
        let result =JSON.parse(request.responseText);
        slides = result.slides;
    };
    request.send();
}

function play(){
    intervals = [];
    for (let slide_index in slides){
        let interval;
        let slide = slides[slide_index];
        interval = setTimeout(function(){changePictureFromFile(slide.url, slide_index)},1000*slide.time);
        intervals.push(interval);
    }
}

function pause(){
    if(!stopped) {
        for (let interval of intervals) {
            clearTimeout(interval);
            intervals = [];
        }
        stopped = true;
    }else{
        intervals = [];
        for(let index = currentStep + 1; index < slides.length;index++){
            let slide = slides[index];
            let interval = setTimeout(function(){changePictureFromFile(slide.url, index)},1000*(slide.time-slides[currentStep].time));
            intervals.push(interval);
        }
        stopped = false;
    }
}

function previous(){
    for (let interval of intervals) {
        clearTimeout(interval);
        intervals = [];
    }
    stopped = true;
    if(currentStep === 0)
        currentStep = 0;
    else
        currentStep = currentStep -1;
    changePictureFromFile(slides[currentStep].url,currentStep);
}

function next(){
    for (let interval of intervals) {
        clearTimeout(interval);
        intervals = [];
    }
    stopped = true;
    if(currentStep === slides.length-1)
        currentStep = slides.length - 1;
    else
        currentStep ++;
    changePictureFromFile(slides[currentStep].url,currentStep);
}

function changePictureFromFile(url,step){
    document.getElementById("MAIN").innerHTML = "<iframe src="+url +"> </iframe>";
    currentStep = step;
}