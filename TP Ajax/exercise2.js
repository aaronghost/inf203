"use strict";

function sendtochat(){
    let textvalue = document.getElementById("textedit").value;
    if(textvalue === "")
        return;
    let request = new XMLHttpRequest();
    request.open("POST","chat.php?phrase="+textvalue);
    request.onload = function(){loadDoc()
    };
    request.send();
}

function loadDoc(){
    let request = new XMLHttpRequest();
    request.open("GET","chatlog.txt");
    request.onload = function (){
        document.getElementById("textarea").innerHTML = "";
        let texts = request.responseText.split("\n").reverse();
        for(let text_index in texts) {
            if(texts[text_index] === "")
                continue;
            let lign = "<p>" + texts[text_index] + "<\/p>";
            document.getElementById("textarea").innerHTML += lign;
            if(document.getElementById("textarea").childElementCount === 10)
                break;
        }
    };
    request.send();
}

function timedOutloadDoc(){
    setTimeout(function(){
        loadDoc();
        timedOutloadDoc();
    },1000);
}

window.onload = timedOutloadDoc();