"use strict";

let http = require('http');
let url = require('url');
let fs = require('fs');
let path = require('path');
let queryString = require('querystring');
let PieChart = require("./piechart.js");
let createPieChart = PieChart.createPieChart;

let storagePath = path.join(__dirname,"storage.json");

if(process.argv.length === 2){
    console.log("Please provide a port argument");
}else{
    let port = process.argv[2];
    console.log("Server launched on port " + port);
    http.createServer(generateServerResponse).listen(port);
}

function generateServerResponse(req, res){
    try {
        let query = url.parse(req.url);
        //case of the root path
        if (query.pathname === '/') {
            res.writeHead(200, {'Content-Type': 'text/html'});
            res.write("This is a valid server address.");
            res.end();
        } else {
            //case of all the other instructions
            const normalizedPath = query.pathname.normalize();
            const queryRegister = normalizedPath.split("/")[1];
            console.log("Function asked : "+queryRegister);
            //Checking for root dir in the normalized path
            if (queryRegister === "exit") {
                res.writeHead(200, {'Content-Type': 'text/html'});
                res.write("The server will stop now.");
                res.end(exitCallBack);
                //file serving
            } else if(queryRegister === "files"){
                const fileLocalPath = path.join(__dirname,query.pathname.substring(6));
                //Checking for root dir in the normalized path
                if(fileLocalPath.indexOf(__dirname) !== 0){
                    raise404(res);
                }else {
                    const file_exists = fs.existsSync(fileLocalPath);
                    //If it does not exist, raise 404
                    if (file_exists === false) {
                        raise404(res);
                    }
                    //Else serve the page
                    else {
                        //Get file content
                        const fileContent = fs.readFileSync(fileLocalPath);
                        //get MIME type for file
                        const MIMEtype = returnCorrectMime(fileLocalPath);
                        //write correct response
                        res.writeHead(200, {'Content-Type': MIMEtype});
                        res.write(fileContent);
                        res.end();
                    }
                }
            } else if(queryRegister === "show"){
                returnJSON(res);
            } else if(queryRegister ==="add"){
                let segmentsArray = JSON.parse(fs.readFileSync(storagePath,"utf8"));
                const parameterDictionary = convertParameters(sanitize(queryString.unescape(query.search)));
                segmentsArray.push({title: parameterDictionary["title"],color:parameterDictionary["color"],value:parseInt(parameterDictionary["value"])});
                fs.writeFileSync(storagePath,JSON.stringify(segmentsArray));
                returnJSON(res);
            } else if(queryRegister === "remove") {
                const segmentsArray = JSON.parse(fs.readFileSync(storagePath,"utf8"));
                const parameterDictionary = convertParameters(sanitize(queryString.unescape(query.search)));
                segmentsArray.splice(parameterDictionary["index"],1);
                fs.writeFileSync(storagePath,JSON.stringify(segmentsArray));
                returnJSON(res);
            } else if(queryRegister === "clear"){
                fs.writeFileSync(path.join(__dirname, "storage.json"), "[{\"title\":\"empty\",\"color\":\"red\",\"value\":1}]");
                returnJSON(res);
            } else if(queryRegister === "restore"){
                fs.writeFileSync(path.join(__dirname, "storage.json"), "[{\"title\":\"1\",\"color\":\"red\",\"value\":0.5},{\"title\":\"2\",\"color\":\"blue\",\"value\":0.25},{\"title\":\"3\",\"color\":\"green\",\"value\":0.25}]");
                returnJSON(res);
            } else if(queryRegister === "pie") {
                const jsonParsedText = JSON.parse(fs.readFileSync(storagePath,"utf8"));
                console.log("JSON storage state: " + jsonParsedText);
                res.writeHead(200,{"Content-Type":"image/svg+xml"});
                let pieChart = createPieChart(jsonParsedText);
                res.write(pieChart);
                res.end();
            } else if(queryRegister === "localpie") {

            } else {
                raise404(res);
            }
        }
    }catch(error) {
        console.error(error);
    }
}

function returnJSON(res){
    const jsonText = fs.readFileSync(storagePath);
    console.log("JSON storage state: " + jsonText);
    res.writeHead(200,{"Content-Type":'application/json'});
    res.write(jsonText);
    res.end();
}

function sanitize(str){
    return str.replace(/<[^>]*>?/gm, '');
}
function convertParameters(str){
    let parameterDictionary = {};
    const stringWithoutQuestionMark = str.substring(1);
    const parameterStrings = stringWithoutQuestionMark.split("&");
    for(const parameterString of parameterStrings){
        const keyValue = parameterString.split("=");
        console.log("Parameter in the query : "+keyValue[0] +" of value : "+keyValue[1]);
        parameterDictionary[keyValue[0]] = keyValue[1];
    }
    return parameterDictionary;
}

function exitCallBack(){
    process.exit(0);
}

function raise404(res){
    res.writeHead(404,{'Content-Type':'text/html'});
    res.write("Page not found on this server");
    res.end();
}

function returnCorrectMime(str){
    const splittedTypes = str.split(".");
    const last_extension = splittedTypes[splittedTypes.length - 1];
    switch (last_extension) {
        case "htm":
        case "html":
            return "text/html";
        case "css":
            return "text/css";
        case "jpg":
        case "jpeg":
            return "image/jpeg";
        case "js":
            return "application/javascript";
        case "gif":
            return "image/gif";
        case "json":
            return "application/json";
        case "pdf":
            return "application/pdf";
        case "txt":
            return "text/plain";
        case "zip":
            return "application/zip";
        case "csv":
            return "image/svg+xml";
    }
}

