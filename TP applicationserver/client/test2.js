"use strict";

let head = document.getElementsByTagName('head')[0];
let script = document.createElement('script');
script.type = 'text/javascript';
script.src = 'piechart.js';
head.appendChild(script);

function showNewSegment(){
    document.getElementById("removeform").style.display = 'none';
    if(document.getElementById("validform").style.display === 'none')
        document.getElementById("validform").style.display = 'block';
    else
        document.getElementById("validform").style.display = 'none';
    document.getElementById("titleTF").value = "";
    document.getElementById("valueTF").value = 0;
    document.getElementById("colorTF").value = "black";
}

function showRemoveSegment(){
    document.getElementById("validform").style.display = 'none';
    if(document.getElementById("removeform").style.display === "none")
        document.getElementById("removeform").style.display = 'block';
    else
        document.getElementById("removeform").style.display = 'none';
    document.getElementById("indexTF").value = 0;
}

function show(){
    let request = new XMLHttpRequest();
    request.open("GET","../../show",false);
    request.onreadystatechange = function (){
        if(request.readyState === 4){
            if(request.status === 200){
                document.getElementById("MAINSHOW").innerHTML = request.responseText;
            }
        }
    };
    request.send();
}

function add(){
    const title = document.getElementById("titleTF").value;
    const value = document.getElementById("valueTF").value;
    const color = document.getElementById("colorTF").value;
    let request = new XMLHttpRequest();
    request.open("GET","../../add?title="+title+"&value="+value+"&color="+ encodeURIComponent(color),false);
    request.onreadystatechange = function (){
        if(request.readyState === 4){
            if(request.status === 200){
                console.log(request.responseText);
                document.getElementById("MAINSHOW").innerHTML = request.responseText;
            }
        }
    };
    request.send();
    showNewSegment();
}

function remove() {
    const index =document.getElementById("indexTF").value;
    let request = new XMLHttpRequest();
    request.open("GET","../../remove?index="+index,false);
    request.onreadystatechange = function (){
        if(request.readyState === 4){
            if(request.status === 200){
                document.getElementById("MAINSHOW").innerHTML = request.responseText;
            }
        }
    };
    request.send();
    showRemoveSegment();
}

function clearJSON(){
    let request = new XMLHttpRequest();
    request.open("GET","../../clear",false);
    request.onreadystatechange = function (){
        if(request.readyState === 4){
            if(request.status === 200){
                document.getElementById("MAINSHOW").innerHTML = request.responseText;
            }
        }
    };
    request.send();
}

function restore(){
    let request = new XMLHttpRequest();
    request.open("GET","../../restore",false);
    request.onreadystatechange = function (){
        if(request.readyState === 4){
            if(request.status === 200){
                document.getElementById("MAINSHOW").innerHTML = request.responseText;
            }
        }
    };
    request.send();
}

function pie(){
    let request = new XMLHttpRequest();
    request.open("GET","../../pie",false);
    request.onreadystatechange = function (){
        if(request.readyState === 4){
            if(request.status === 200){
                document.getElementById("MAINSHOW").innerHTML = request.responseText;
            }
        }
    };
    request.send();
}

function localpie(){
    let request = new XMLHttpRequest();
    request.open("GET","../../show",false);
    request.onreadystatechange = function (){
        if(request.readyState === 4){
            if(request.status === 200){
                const JSONfile = request.responseText;
                const parsedJSON = JSON.parse(JSONfile);
                document.getElementById("MAINSHOW").innerHTML = createPieChart(parsedJSON);
            }
        }
    };
    request.send();}