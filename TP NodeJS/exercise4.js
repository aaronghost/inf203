"use strict";

let ex3 = require('./exercise3');
let fs = require('fs');
let Student = ex3.Student;
let ForeignStudent = ex3.ForeignStudent;

class Promotion{
    constructor() {
        this.array = [];
    }

    add(student){
        this.array.push(student);
    }

    size(){
        return this.array.length;
    }

    get(i){
        return this.array[i];
    }

    print(){
        let str ="";
        for (const student of this.array){
            str += student.toString()+"\n";
        }
        return str;
    }

    write(){
        return JSON.stringify(this.array);
    }

    read(str) {
        const JSONResult = JSON.parse(str);
        for (let student of JSONResult) {
            if (student.nationality === undefined) {
                student = Object.assign(new Student(),student);
            }
            else {
                student = Object.assign(new ForeignStudent(),student);
            }
            this.array.push(student);
        }
    }

    saveToFile(fileName){
        fs.writeFile(fileName,this.write(),(err) => {
            if(err)
                console.log("Error");
        });
    }

    readFromFile(fileName){
        fs.readFile(fileName,(err, data) => {
            if(err)
                console.log("Error");
            else
                this.read(data);
        });
    }
}

exports.Promotion = Promotion;
