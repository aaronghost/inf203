"use strict";

function fibonacciIt(n){
    if(n === 0){
        return 0;
    }else {
        let current_result = 1;
        let former_result = 0;
        for (let i = 2; i <= n; i++) {
            const temp = current_result;
            current_result = current_result + former_result;
            former_result = temp;
        }
        return current_result;
    }
}

function fibonacciRec(n){
    if(n === 0) {
        return 0;
    }else if(n === 1){
        return 1;
    }else{
        return fibonacciRec(n-1)+fibonacciRec(n-2);
    }
}

function fibonacciArray(arr){
    let rep = [];
    for(let i = 0; i < arr.length; i++){
        rep.push(fibonacciRec(arr[i]));
    }
    return rep;
}

function fibonacciMap(arr){
    return arr.map(n => fibonacciRec(n));
}

exports.fibonacciIt = fibonacciIt;
exports.fibonacciRec = fibonacciRec;
exports.fibonacciArray = fibonacciArray;
exports.fibonacciMap = fibonacciMap;