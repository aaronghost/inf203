"use strict";

function wordCount(str){
    let dict = {};
    const list = str.split(" ");
    for (const name of list){
        if(name in dict) {
            dict[name] = dict[name] + 1;
        }else{
            dict[name] = 1;
        }
    }
    return dict;
}

class WordList{
    constructor(str){
        //first get the words and their count
        this.wordsdict = wordCount(str);
        //get a sorted list of the words
        this.wordarray = [];
        for (const key in this.wordsdict) {
            this.wordarray.push(key);
        }
        this.wordarray = this.wordarray.sort();
        //compute min and max ans as the array is sorted, we only replace the value if it is strictly more "interesting"
        let min = this.wordsdict[this.wordarray[0]];
        let max =  this.wordsdict[this.wordarray[0]];
        this.minWord = this.wordarray[0];
        this.maxWord = this.wordarray[0];
        for (const name of this.wordarray){
            if (this.wordsdict[name] > max ){
                max = this.wordsdict[name];
                this.maxWord = name;
            }else if(this.wordsdict[name] < min){
                min = this.wordsdict[name];
                this.minWord = name;
            }
        }
    }

    maxCountWord(){
        return this.maxWord;
    }

    minCountWord(){
        return this.minWord;
    }

    getWords(){
        return this.wordarray;
    }

    getCount(word){
        return this.wordsdict[word];
    }

    applyWordFunc(f){
        return this.wordarray.map(x => f(x))
    }
}

exports.wordCount = wordCount;
exports.WordList = WordList;